import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Validators } from '@angular/forms';
import { ApiService} from '../../services/Api/api.service';

@Component({
  selector: 'app-shipping-address',
  templateUrl: './shipping-address.component.html',
  styleUrls: ['./shipping-address.component.css']
})
export class ShippingAddressComponent implements OnInit {
  formData: FormGroup;
  colonies: Array<Object> = [1];
  result = false;
  constructor(private api: ApiService,
              private from: FormBuilder) {
  }

  ngOnInit(): void {
      this.formData = this.from.group({
        name: [null, Validators.required],
        lastName: [null, Validators.required],
        email: [null, [Validators.required, Validators.email]],
        phone: [null, Validators.required],
        zip: ['', Validators.required],
        colonie: ['', Validators.required],
        state: [null, Validators.required],
        city: [null, Validators.required],
        town: [null, Validators.required],
        street: [null, Validators.required],
        facturacion: [false, Validators.required]
        });
      }
    clean(){
      this.formData.controls['name'].setValue("");
      this.formData.controls['lastName'].setValue("");
      this.formData.controls['email'].setValue("");
      this.formData.controls['phone'].setValue("");
      this.formData.controls['zip'].setValue("");
      this.formData.controls['colonie'].setValue("");
      this.formData.controls['town'].setValue("");
      this.formData.controls['city'].setValue("");
      this.formData.controls['state'].setValue("");
      this.formData.controls['street'].setValue("");
      this.formData.controls['facturacion'].setValue('');
    }

    getAddress(zip) {
      this.api.getAddressByZip(zip).subscribe(data => {
        this.colonies = data.colonies;
        if (this.colonies.length > 1){
          this.formData.controls['colonie'].setValue('');
        } else {
          this.formData.controls['colonie'].setValue(data.colonies[0]);
        }
        this.formData.controls['city'].setValue(data.city);
        this.formData.controls['town'].setValue(data.town);
        this.formData.controls['state'].setValue(data.state);

      });
    }
    sendContanct(formData: Object){
      this.api.SendContanct(formData).subscribe( data => {
        this.result = true;
      });
    }

  close(){
    this.clean();
    this.result = false;
  }

}
