import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {
  @Input() title: string;
  @Input() message: string;
  @Output() closeModal = new EventEmitter<string>();
  constructor() { }

  ngOnInit(): void {
  }

  close(): void {
    document.querySelector('#modal').classList.remove('modal-open');
    this.closeModal.emit();
  }
}
