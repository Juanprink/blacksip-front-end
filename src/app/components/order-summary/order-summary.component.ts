import { Component, Input, OnInit, OnChanges } from '@angular/core';

@Component({
  selector: 'app-order-summary',
  templateUrl: './order-summary.component.html',
  styleUrls: ['./order-summary.component.css']
})
export class OrderSummaryComponent implements OnInit, OnChanges {

  @Input() products: Array<any>;
   sumTotal: number;
  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(): void {
    if(this.products !== undefined){
      this.sumTotal = this.products.map(product => parseInt(product.price)).reduce((a, b) => a + b, 0);
    }
  }

}
