import {Component, OnInit} from '@angular/core';
import {ApiService} from './services/Api/api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'app';
  products: Array<any>;
  constructor(private api: ApiService) { }

  ngOnInit(): void {
    this.getProducts();
  }


  getProducts() {
    this.api.getProducts().subscribe(data => {
      this.products = data;
    });
  }
}
