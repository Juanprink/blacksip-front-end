import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  url = 'https://blackisp.herokuapp.com';
  constructor(private http: HttpClient) { }

  getAddressByZip(zip): Observable<any>{
    return this.http.get(`${this.url}/postalCodes/${zip}`);
  }
  getProducts(): Observable<any> {
    return this.http.get(`${this.url}/products`);
  }
  SendContanct(formData): Observable<any> {
    return this.http.post(`${this.url}/contact`, formData);
  }
}
